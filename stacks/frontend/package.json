{
  "name": "flockademic-frontend",
  "version": "0.0.0",
  "description": "Flockademic front-end",
  "main": "index.js",
  "scripts": {
    "storybook": "start-storybook -p 9001 -c .storybook",
    "lint": "tslint --project tsconfig.json && stylelint 'src/**/*.scss'",
    "test": "yarn run lint & stylelint 'src/**/*.scss' && jest",
    "start": "yarn run server --open",
    "server": "webpack-dev-server --config=webpack.dev.config.js --hot",
    "build": "webpack -p --config=webpack.prd.config.js",
    "deploy": "node build/deploy.js",
    "undeploy": "node build/deploy.js undeploy",
    "awaitDeployTasks": "node build/deploy.js await",
    "precommit": "cd ../../ && yarn run precommit",
    "all-tests": "cd ../../ && yarn run all-tests"
  },
  "postcss": {
    "plugins": {
      "postcss-cssnext": {
        "features": {
          "customProperties": {
            "warnings": false
          }
        }
      }
    }
  },
  "browserslist": [
    "> 1%",
    "last 2 versions",
    "ie >= 9",
    "and_chr >= 2.3",
    "Firefox ESR"
  ],
  "jest": {
    "clearMocks": true,
    "moduleNameMapper": {
      "\\.(css|scss|jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "<rootDir>/__tests__/__mocks__/fileMock.js"
    },
    "transform": {
      "\\.(tsx|ts)$": "ts-jest"
    },
    "testRegex": "(/__tests__/.*\\.(test|spec))\\.(ts|tsx|js)$",
    "moduleFileExtensions": [
      "ts",
      "tsx",
      "js"
    ],
    "setupFiles": [
      "<rootDir>/__tests__/setupJest.js"
    ],
    "setupTestFrameworkScriptFile": "./node_modules/jest-enzyme/lib/index.js",
    "collectCoverage": true,
    "collectCoverageFrom": [
      "src/**.{js,ts,tsx}",
      "!src/**.d.ts",
      "!src/render.tsx",
      "!../../lib/interfaces/**"
    ],
    "coveragePathIgnorePatterns": [
      "/src/components/prerenderableRouter/component"
    ],
    "coverageReporters": [
      "json",
      "lcov",
      "text",
      "text-summary"
    ],
    "coverageDirectory": "__tests__/__coverage__",
    "coverageThreshold": {
      "global": {
        "branches": 100,
        "functions": 100,
        "lines": 100,
        "statements": 100
      }
    }
  },
  "keywords": [
    "open",
    "access"
  ],
  "author": "Vincent Tunru",
  "license": "AGPL-3.0",
  "devDependencies": {
    "@storybook/react": "^3.3.13",
    "@types/jest": "^23.3.1",
    "@types/luxon": "^0.5.0",
    "@types/node": "^8.10.10",
    "@types/prop-types": "^15.5.2",
    "@types/query-string": "^5.0.1",
    "@types/react": "^16.7.3",
    "@types/react-dom": "^16.0.9",
    "@types/react-helmet": "^5.0.4",
    "@types/react-redux": "^5.0.15",
    "@types/react-router": "^4.4.1",
    "@types/react-router-dom": "^4.3.1",
    "@types/react-transition-group": "^2.0.7",
    "@types/storybook__react": "^3.0.7",
    "aws-sdk": "^2.193.0",
    "clean-webpack-plugin": "^0.1.18",
    "cloudflare": "^1.1.1",
    "css-loader": "^0.28.9",
    "enzyme": "^3.3.0",
    "enzyme-adapter-react-16": "^1.1.1",
    "enzyme-to-json": "^3.3.1",
    "extract-text-webpack-plugin": "^3.0.2",
    "favicons-webpack-plugin": "^0.0.7",
    "glob": "^7.1.2",
    "html-loader": "^0.5.5",
    "html-webpack-plugin": "^2.30.1",
    "husky": "^0.14.3",
    "jest": "^23.5.0",
    "jest-enzyme": "^6.0.0",
    "jest-fetch-mock": "^1.4.1",
    "mime": "^2.2.0",
    "node-sass": "4.9.0",
    "postcss-cssnext": "^3.1.0",
    "postcss-loader": "^2.1.0",
    "raf": "^3.4.0",
    "react-addons-test-utils": "^15.6.2",
    "react-test-renderer": "^16.2.0",
    "robotstxt-webpack-plugin": "^4.0.0",
    "sass-loader": "^7.0.1",
    "source-map-loader": "^0.2.3",
    "storybook-router": "^0.3.2",
    "style-loader": "^0.20.1",
    "stylelint": "^9.2.0",
    "stylelint-config-standard": "^18.1.0",
    "stylelint-webpack-plugin": "^0.10.2",
    "svg-url-loader": "^2.3.1",
    "ts-jest": "<23.10.0",
    "ts-loader": "^3.5.0",
    "tslint": "^5.9.1",
    "tslint-eslint-rules": "^5.0.0",
    "tslint-loader": "^3.5.3",
    "tslint-microsoft-contrib": "^5.0.2",
    "tslint-react": "^3.5.0",
    "typescript": "^2.7.1",
    "url-loader": "^1.0.1",
    "wdio-json-reporter": "^0.3.1",
    "webpack": "^3.11.0",
    "webpack-dev-server": "^2.11.1"
  },
  "engines": {
    "node": "^8.10"
  },
  "dependencies": {
    "axios": "0.18.0",
    "bulma": "0.7.1",
    "core-js": "^2.5.3",
    "localforage": "^1.5.6",
    "luxon": "^1.0.0",
    "material-design-icons": "^3.0.1",
    "prop-types": "^15.6.0",
    "query-string": "^5.1.0",
    "react": "^16.2.0",
    "react-dom": "^16.2.0",
    "react-ga": "^2.4.1",
    "react-helmet": "^5.2.0",
    "react-redux": "^5.0.6",
    "react-router": "4.2.0",
    "react-router-dom": "4.2.0",
    "react-transition-group": "^2.2.1",
    "redux": "^4.0.0",
    "redux-thunk": "^2.2.0",
    "reselect": "^3.0.1"
  }
}
